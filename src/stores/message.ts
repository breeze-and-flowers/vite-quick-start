import {defineStore} from 'pinia'

export const useMsgStore = defineStore({
  id: 'msg',
  state: () => ({
    helloStr: 'hello world'
  }),
  getters: {},
  actions: {}
})