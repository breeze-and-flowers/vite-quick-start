import {createRouter, createWebHistory} from 'vue-router'
import type {Router, RouteRecordRaw} from 'vue-router'

import Home from '@/view/home.vue'

let routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: Home
  }
]

let router: Router = createRouter({
  history: createWebHistory(),
  routes
})

export default router