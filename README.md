# vite-quick-start
vite 项目快速开始<br>
#### 技术栈：
 - vue3
 - vite2
 - vue-router
 - pinia
 - axios
 - typescript
 - scss


### 安装依赖
```angular2html
npm install
```

### 运行
```angular2html
npm run dev
```

### 打包
```angular2html
npm run build
```
