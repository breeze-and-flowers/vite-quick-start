import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {fileURLToPath, URL} from 'url'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    // 设置别名
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  // css预处理器
  css: {
    preprocessorOptions: {
      scss: {
        // 自动导入全局文件
        additionalData: '@import "./src/assets/style/global.scss";'
      }
    },
    postcss: {
      // 自动css前缀
      plugins: [require('autoprefixer')]
    }
  }
})
